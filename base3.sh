#!/bin/bash
setfont ter-u20n
#setfont iso01-12x22.psfu.gz 

timedatectl set-ntp true
hwclock --systohc --localtime
# sudo hwclock --systohc --utc   

reflector --verbose --country France -l 12 -p https --sort rate --save /etc/pacman.d/mirrorlist

firewall-cmd --add-port=1025-65535/tcp --permanent
firewall-cmd --add-port=1025-65535/udp --permanent
firewall-cmd --reload



pacman -S --noconfirm xorg-{server,xinit,xkill,xrandr,xinput,xfontsel,xlsfonts,xwininfo} xf86-{video-vesa,video-fbdev} libwnck3

# pacman -S --noconfirm xorg sddm qt5-graphicaleffects qt5-quickcontrols2 qt5-svg plasma kde-applications simplescreenrecorder materia-kde

pacman -S --noconfirm xfce4 xfce4-goodies quodlibet file-roller parole ristretto thunar-media-tags-plugin

# pacman -S --noconfirm xfce4 xfce4-{goodies,battery-plugin,datetime-plugin,mount-plugin,netload-plugin,notifyd,pulseaudio-plugin,screensaver,screenshooter,taskmanager,wavelan-plugin,weather-plugin,whiskermenu-plugin,xkb-plugin} quodlibet vlc file-roller parole ristretto thunar-media-tags-plugin meld obs-studio

# pacman -S --needed xfce4 parole ristretto thunar-archive-plugin thunar-media-tags-plugin xfce4-battery-plugin xfce4-datetime-plugin xfce4-mount-plugin xfce4-netload-plugin xfce4-notifyd xfce4-pulseaudio-plugin xfce4-screensaver xfce4-taskmanager xfce4-wavelan-plugin xfce4-weather-plugin xfce4-whiskermenu-plugin xfce4-xkb-plugin file-roller network-manager-applet leafpad epdfview galculator lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings capitaine-cursors arc-gtk-theme 

# pacman -S --noconfirm lightdm-{gtk-greeter,gtk-greeter-settings}

# pacman -R --noconfirm mousepad

ln -sf /usr/share/zoneinfo/Europe/Paris /etc/localtime
hwclock --systohc --localtime
sed -i 's/#\(fr_FR\.UTF-8\)/\1/' /etc/locale.gen
locale-gen  
echo "LANG=fr_FR.UTF-8" >> /etc/locale.conf 
echo "KEYMAP=fr-latin9" >> /etc/vconsole.conf
echo "arch" >> /etc/hostname
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1       localhost" >> /etc/hosts
echo "127.0.1.1 arch.localhost arch" >> /etc/hosts


mkdir -p /usr/share/sddm/themes
# git clone https://framagit.org/MarianArlt/sddm-sugar-candy.git /usr/share/sddm/themes/sugar-candy
echo "[Theme]" >> /etc/sddm.conf
echo "Current=sugar-candy" >> /etc/sddm.conf
echo "CursorTheme=Breeze" >> /etc/sddm.conf
mkdir -p /usr/lib/sddm/sddm.conf.d
echo "[Theme]" >> /usr/lib/sddm/sddm.conf.d/sddm.conf
echo "Current=sugar-candy" >> /usr/lib//sddm/sddm.conf.d/sddm.conf
echo "CursorTheme=Breeze" >> /usr/lib//sddm/sddm.conf.d/sddm.conf
# systemctl enable lightdm
pacman -S --needed --noconfirm sddm qt5-graphicaleffects qt5-quickcontrols2 qt5-svg
git clone  https://gitlab.com/1del/sugar-candy.git /usr/share/sddm/themes/sugar-candy
rm -r /usr/share/sddm/themes/sugar-candy/.git
systemctl enable sddm
localectl set-x11-keymap fr
xdg-user-dirs-gtk-update --noconfirm



