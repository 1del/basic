#!/bin/bash
#setfont ter-u20n
#setfont iso01-12x22.psfu.gz

sudo timedatectl set-ntp true
sudo hwclock --systohc --localtime
# sudo hwclock --systohc --UTC   

sudo reflector --verbose --country France -l 12 -p https --sort rate --save /etc/pacman.d/mirrorlist

sudo firewall-cmd --add-port=1025-65535/tcp --permanent
sudo firewall-cmd --add-port=1025-65535/udp --permanent
sudo firewall-cmd --reload

git clone https://aur.archlinux.org/pikaur.git
cd pikaur/
makepkg -si --noconfirm

git clone https://aur.archlinux.org/yay
cd yay
makepkg -sri --noconfirm

#pikaur -S --noconfirm system76-power
#sudo systemctl enable --now system76-power
#sudo system76-power graphics integrated
#pikaur -S --noconfirm gnome-shell-extension-system76-power-git
#pikaur -S --noconfirm auto-cpufreq
#sudo systemctl enable --now auto-cpufreq

sudo pikaur -S --noconfirm hypnotix google-chrome pamac-all mugshot
# sudo pikaur -S --noconfirm  pyakm
# sudo flatpak install -y spotify
sudo flatpak install -y transmission-qt
# sudo flatpak install -y kdenlive
sudo pacman -S --noconfirm --needed fuse2 gtkmm pcsclite libcanberra 
yay -S --noconfirm --needed ncurses5-compat-libs
yay -S --noconfirm --needed  vmware-workstation
sudo /usr/lib/vmware/bin/licenseTool enter YF390-0HF8P-M81RQ-2DXQE-M2UT6 "" "" 16.0+ "VMware Workstation" /usr/lib/vmware
sudo systemctl enable vmware-networks.service
sudo systemctl enable vmware-usbarbitrator.service
sudo modprobe -a vmw_vmci vmmon
# sudo vmware-installer --uninstall-product vmware-workstation
# sudo systemctl enable lightdm
sudo localectl set-x11-keymap fr
/bin/echo -e "\e[1;32mREBOOTING IN 5..4..3..2..1..\e[0m"
sleep 5
sudo reboot
