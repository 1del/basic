#!/bin/bash
setfont ter-u20n
#setfont iso01-12x22.psfu.gz 

# mkfs.fat -F32 -n EFI /dev/sda1
# mkswap -L swap /dev/sda2
# swapon /dev/sda4
# mkfs.ext4 -L arch /dev/sda9
# mkfs.ext4 -L home /dev/sda10
mount /dev/sda9 /mnt
mkdir /mnt/{boot,boot/efi} && mount /dev/sda1 /mnt/boot/efi
mkdir /mnt/home && mount /dev/sda10 /mnt/home


dhclient -v
timedatectl set-ntp true
reflector --verbose --country France -l 12 -p https --sort rate --save /etc/pacman.d/mirrorlist

pacstrap /mnt base base-devel linux-hardened linux-hardened-headers linux-firmware python lsb-release git p7zip unrar tar rsync dnsutils net-tools ntfs-3g modemmanager network-manager-applet networkmanager networkmanager-{openconnect,vpnc} wireless_tools wpa_supplicant gnome-keyring nss-mdns usb_modeswitch whois dhclient ethtool xf86-video-intel xf86-video-vmware xf86-input-synaptics alsa-{utils,plugins,firmware,oss} pulseaudio pulseaudio-alsa pavucontrol asoundconf tlp upower sof-firmware efibootmgr dosfstools mtools intel-ucode os-prober grub hardinfo ncdu bleachbit neofetch hwinfo wget s-tui tldr firefox-i18n-fr firefox-ublock-origin nano htop gtop man ttf-{dejavu,liberation,bitstream-vera,opensans,roboto,roboto-mono} noto-fonts opendesktop-fonts gst-plugins-{base,good,bad,ugly} gst-libav dhcpcd ntp acpid cronie cups gimp gimp-help-fr hplip python-pyqt5 gvfs-{afc,goa,google,gphoto2,mtp,nfs,smb} xdg-user-dirs-gtk epdfview gnome-disk-utility gnome-calculator papirus-icon-theme archlinux-wallpaper gtk-update-icon-cache bash-completion openssh reflector terminus-font firewalld flatpak samba numlockx xdotool catfish mlocate mesa ifplugd dialog 


genfstab -U -p /mnt >> /mnt/etc/fstab
arch-chroot /mnt




