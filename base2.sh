#!/bin/bash
#setfont ter-u20n
#setfont iso01-12x22.psfu.gz

# git clone https://github.com/kouros17/Adwaita-Maia-Dark.git /usr/share/themes/Adwaita-Maia-Dark
mount | grep efivars &> /dev/null || mount -t efivarfs efivarfs /sys/firmware/efi/efivars
grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=Arch --recheck
#mkdir /boot/efi/EFI/boot
cp /boot/efi/EFI/Arch/grubx64.efi /boot/efi/EFI/boot/bootx64.efi
echo " " >> /etc/default/grub
echo "# Uncomment to disable generation of recovery mode menu entries" >> /etc/default/grub
echo "GRUB_DISABLE_RECOVERY=false" >> /etc/default/grub
echo " " >> /etc/default/grub
echo "#New GRUB update disables OS prober by default. We don't want that." >> /etc/default/grub
echo "GRUB_DISABLE_OS_PROBER=false" >> /etc/default/grub
echo " " >> /etc/default/grub
echo "# Uncomment one of them for the gfx desired, a image background or a gfxtheme" >> /etc/default/grub
echo '#GRUB_BACKGROUND="/usr/share/grub/background.png' >> /etc/default/grub
echo 'GRUB_THEME="/usr/share/grub/themes/arch/theme.txt"' >> /etc/default/grub

git clone https://gitlab.com/1del/arch.git /usr/share/grub/themes/arch
rm -r /usr/share/grub/themes/arch/.git
grub-mkconfig -o /boot/grub/grub.cfg
git clone https://gitlab.com/1del/yaru.git /usr/share/sounds/Yaru/
rm -r /usr/share/sounds/Yaru/.git
rm -r /etc/skel
git clone https://gitlab.com/1del/skel.git /etc/skel
rm -r /etc/skel/.git
git clone https://gitlab.com/1del/adwaita.git /tmp/adwaita/
rm -r /tmp/adwaita/.git
cp -r /tmp/adwaita/* /usr/share/themes
rm -r /tmp/adwaita
git clone https://gitlab.com/1del/icons.git /tmp/icons
rm -r /tmp/icons/.git
cp -r /tmp/icons/* /usr/share/icons
rm -r /tmp/icons
cp -r /etc/skel/./ /root/
git clone https://gitlab.com/1del/archlinux.git /tmp/archlinux/
rm -r /tmp/archlinux/.git
cp -r /tmp/archlinux/* /usr/share/backgrounds/archlinux/
echo root:a | chpasswd
useradd -m -G audio,video,input,wheel,sys,log,rfkill,lp,adm -c 'Dell' -s /bin/bash dell
echo dell:a | chpasswd
usermod -aG wheel dell

echo "%wheel ALL=(ALL) ALL" >> /etc/sudoers.d/installer

# gtk-update-icon-cache /usr/share/icons/Papirus-Adapta-Maia
# gtk-update-icon-cache /usr/share/icons/Papirus-Adapta-Nokto-Maia
# gtk-update-icon-cache /usr/share/icons/Papirus-Dark-Maia
# gtk-update-icon-cache /usr/share/icons/Papirus-Light-Maia
# gtk-update-icon-cache /usr/share/icons/Papirus-Maia

systemctl enable NetworkManager
systemctl enable ntpd
# systemctl enable bluetooth
systemctl enable cups.service
systemctl enable sshd
systemctl enable avahi-daemon
systemctl enable tlp 
systemctl enable reflector.timer
systemctl enable fstrim.timer
systemctl enable libvirtd
systemctl enable firewalld
systemctl enable acpid
